项目目前主要包括六个文件:

1. Qwindow.py:在这个文件中是使用QT designer生成软件的UI文件之后，通过转换得到的python类代码，里面主要是整体界面和一些按键的布局设置

2. CET.py:这个文件是主要的代码文件，对每一个UI组件都关联了相应的函数，比如按下ENTER取色后会在九宫格取色盘里记录下来，实现取色历史的功能等

3. Color_extraction_tool：Linux下的项目产出单可执行文件

4. Color_extraction_tool.exe：Windows下的项目产出单可执行文件

5. Color_extraction_tool.tar.gz: Linux下的项目产出的含资源包类型的文件

6. test.jpg：本地取色功能测试图片
